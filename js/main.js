// фильтр и loadMore
$(function() {
	var item = $('.portfolio-item'),
		load = $('.js-loadMore'),
		filterAll = $('.js-filter-all'),
		filter = $('.js-filter'),
		allFilter = $('.js-filter, .js-filter-all'),
		active = 'active';

	item.slice(0, 6).show();

	load.on('click', function(e) {
		e.preventDefault();
		if (item.filter(':hidden').length === 0) {
			item.fadeOut('slow');
			item.slice(0, 6).fadeIn('slow');
			load.text('SHOW MORE');
			$('html,body').animate(
				{
					scrollTop: $('#portfolio').offset().top
				},1500);
		} else {
			item
				.filter(':hidden')
				.slice(0, 3)
				.slideDown();
				$('html,body').animate(
					{
						scrollTop: ($(this).offset().top  - $(window).height()/2)
					},1500);
			if (item.filter(':hidden').length === 0) {
				load.text('hide');
			}

			setTimeout(function () {
				$('html,body').animate(
					{
						scrollTop: ($(this).offset().top  - $(window).height()/2)
					},
					1500
				);
			}, 1000);


		}
	});

	filterAll.on('click', function(e) {
		e.preventDefault();
		filter.removeClass(active);
		$(this).addClass(active);
		item.fadeOut(1);
		item.slice(0, 6).fadeIn('slow');
		load.fadeIn();
	});

	filter.on('click', function(e) {
		e.preventDefault();
		allFilter.removeClass(active);
		$(this).addClass(active);
		item.fadeOut(1);
		var id = $(this).data('class');
		$('.js-item' + id).fadeIn('slow');
		load.fadeOut();
	});
});


// Слайдер отзывов
$(function () {
	$('.testimonials__slider-img').slick({
		centerMode: true,
		centerPadding: '0',
		slidesToShow: 3,
		infinite: true,
		arrows: false,
		focusOnSelect: true,
		asNavFor: '.testimonials__slider-text',
	});

	$('.testimonials__slider-text').slick({
		dots: true,
		fade: true,
		cssEase: 'linear',
		dotsClass: 'js-slick-dots',
		arrows: false,
		infinite: true,
		asNavFor: '.testimonials__slider-img',
	});

	$('.leftArrow').on('click', function () {
		$('.testimonials__slider-text').slick('slickPrev');
	});
	$('.rightArrow').on('click', function () {
		$('.testimonials__slider-text').slick('slickNext');
	});

});

// мобильное меню
$(function () {
	var menuBtn = document.querySelector(".js-menuBtn"),
		menu = document.querySelector(".header__menu"),
		menuLink = menu.getElementsByTagName("a");

	function toggleMenu() {
		menu.classList.toggle("active");
		menuBtn.classList.toggle("active");
	}

	menuBtn.onclick = function(event) {
		event.preventDefault();
		toggleMenu();
	};

	for (var i = 0; i < menuLink.length; i++) {
		menuLink[i].addEventListener("click", function() {
			toggleMenu();
		});
	}
});

// плавный скролл
$(function () {
	$(".link").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 900);
	});
});

// wow animation
wow = new WOW(
	{
		mobile: false,
	}
)
wow.init();